import $C from '../plugins/$C';
import helper from '../plugins/helper';

export const state = () => ({
  departmentList: [],
  doctorList: [],
  selectedDoctor: {},
});

export const mutations = {
  updateDepartmentList(state, departmentList) {
    state.departmentList = departmentList;
  },
  updateDoctorList(state, doctorList) {
    state.doctorList = doctorList;
  },
  updateSelectedDoctor(state, selectedDoctor) {
    state.selectedDoctor = selectedDoctor;
  },
  cleanDepartmentList(state) {
    state.departmentList = [];
  },
  cleanDoctorList(state) {
    state.doctorList = [];
  },
  cleanSelectedDoctor(state) {
    state.selectedDoctor = {};
  },
};

export const actions = {
  async refresh({ commit, rootState }) {
    try {
      const params = {
        token: rootState.corsuser.user.token,
        pageNo: '1',
        pageSize: $C.pageSize,
        hospitalId: rootState.referral.hospital.receiveHospital,
      };
      const res = await helper.POST($C.medSys + 'v1/referral/dictReferDept/listAll', params);
      if (!parseInt(res.status)) {
        commit('updateDepartmentList', res.result);
      }
    } catch (e) {
      console.log(e);
    }
  },
  async loadMore({ commit, rootState, state }, index) {
    try {
      const params = {
        token: rootState.corsuser.user.token,
        pageNo: index + 1,
        pageSize: $C.pageSize,
      };
      if (state.searchText) {
        params.name = state.searchText;
      }
      const res = await helper.POST($C.medSys + 'v1/referral/dictReferDept/listAll', params);
      if (!parseInt(res.status)) {
        commit('updateDepartmentList', state.departmentList.concat(res.result));
      }
    } catch (e) {
      console.log(e);
    }
  },
  async selectDepartment({ commit, rootState }, deptId) {
    try {
      const params = {
        token: rootState.corsuser.user.token,
        deptId,
      };
      const res = await helper.POST($C.medSys + 'v1/referral/dictReferDoctor/listAll', params);
      if (!parseInt(res.status)) {
        commit('updateDoctorList', res.result);
      }
    } catch (e) {
      console.log(e);
    }
  },
};

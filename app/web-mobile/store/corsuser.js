export const state = () => ({
  user: {},
});

export const mutations = {
  updateUser(state, user) {
    state.user = user;
  },
  deleteUser(state) {
    state.user = '';
  },
};

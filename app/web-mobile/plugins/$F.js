/* eslint-disable no-sequences */
import $C from './$C';
// import helper from './helper';
import Vue from 'vue';
import axios from '../plugins/axios';
import R from 'ramda';
import qs from 'qs';
import errorMsg from './error.json';
import { Dialog, Toast } from 'quasar-framework';
// 错误信息预处理
Vue.prototype.jump = function(to) {
  if (this.$router) {
    this.$router.push(to);
  }
},
Vue.prototype.back = function() {
  if (this.$router) {
    this.$router.back();
  }
},
Vue.prototype.go = function(index) {
  this.$router.go(index);
},

Vue.prototype.preError = function(response) {
  const self = this;
  const data = JSON.parse(response.data);
  const error = data.status;
  let msg = '';
  if (!parseInt(data.status)) {
    return data.result;
  }
  // 这里对一些特殊错误信息进行预处理
  function handleError(error) {
    switch (parseInt(error)) {
      case 102:self.jump($C.loginPath); break;
      default:
        break;
    }
  }
  if (data.message) {
    Dialog.create({ title: '错误', message: data.message,
      buttons: [{ label: '确定', handler() { handleError(error); } }] });
  } else {
    msg = errorMsg[error] ? errorMsg[error] : error;
    if (msg) {
      Dialog.create({ title: '错误', message: msg,
        buttons: [{ label: '确定', handler() { handleError(error); } }] });
    }// 弹出错误
  }
};
Vue.prototype.preGET = async function(url, data, option) {
  try {
    if (!option) { option = {}; }
    if (data) { option.params = data; }
    const response = await axios.get(url, option);
    return this.preError(response);
  } catch (e) {
    Dialog.create({ title: '错误', message: e.response.status,
      buttons: [{ label: '确定', handler() {} }] });
    return e;
  }
};
Vue.prototype.GET = async function(url, data, option) {
  try {
    if (!option) { option = {}; }
    if (data) { option.params = data; }
    const response = await axios.get(url, option);
    return JSON.parse(response.data);
  } catch (e) {
    return e;
  }
};

Vue.prototype.POST = async function(url, data, option) {
  try {
    if (!option) { option = {}; }
    if (data) { data = qs.stringify(data); }
    const response = await axios.post(url, data);
    return JSON.parse(response.data);
  } catch (e) {
    return e;
  }
};

Vue.prototype.prePOST = async function(url, data, option) {
  try {
    if (!option) { option = {}; }
    if (data) { data = qs.stringify(data); }
    const response = await axios.post(url, data);
    return this.preError(response);
  } catch (e) {
    Dialog.create({ title: '错误', message: e.response.status,
      buttons: [{ label: '确定', handler() {} }] });
    return e;
  }
};

Vue.prototype.checkLogin = function() {
  try {
    let user = localStorage.getItem('corsUser');
    // let user = helper.getCookie('corsUser');
    if (user && user !== '') {
      user = JSON.parse(user);
      return user;
    }
    user = this.$store.state.corsuser.user;
    if (user === '' || !user.token) return null;
    return R.clone(user);
  } catch (e) {
    return null;
  }
};
// 检查是否登录返回登录用户信息
Vue.prototype.ensureLogin = function() {
  const self = this;
  const user = self.checkLogin();
  if (user && user.token) {
    this.$store.commit('corsuser/updateUser', user);
    return user;
  }
  self.jump($C.loginPath);
};

Vue.prototype.persistUser = function(user) {
  this.$store.commit('corsuser/updateUser', user);
  // helper.setCookie('corsUser', JSON.stringify(user));
  localStorage.setItem('corsUser', JSON.stringify(user));
  return true;
};

Vue.prototype.systemLogout = function() {
  this.$store.commit('corsuser/deleteUser');
  // helper.setCookie('corsUser', '', -1);
  localStorage.removeItem('corsUser');
};

Vue.prototype.notUsed = async function(info) {
  info = info ? info : '暂未开放！';
  Toast.create({
    html: info,
    icon: 'warning',
    timeout: 2000,
  });
};

import $C from '../plugins/$C';
import helper from '../plugins/helper';

export const state = () => ({
  hospitalList: [],
  searchText: '',
});

export const mutations = {
  update(state, hospitalList) {
    state.hospitalList = hospitalList;
  },
  clean(state) {
    state.hospitalList = [];
  },
  search(state, searchText) {
    state.searchText = searchText;
  },
};

export const actions = {
  async refresh({ commit, rootState, state }) {
    try {
      const params = {
        token: rootState.corsuser.user.token,
        pageNo: '1',
        pageSize: $C.pageSize,
      };
      if (state.searchText) {
        params.name = state.searchText;
      }
      const res = await helper.POST($C.medSys + 'v1/referral/dictReferHospital/list', params);
      if (!parseInt(res.status)) {
        commit('update', res.result);
      }
    } catch (e) {
      console.log(e);
    }
  },
  async loadMore({ commit, rootState, state }, index) {
    try {
      const params = {
        token: rootState.corsuser.user.token,
        pageNo: index + 1,
        pageSize: $C.pageSize,
      };
      if (state.searchText) {
        params.name = state.searchText;
      }
      const res = await helper.POST($C.medSys + 'v1/referral/dictReferHospital/list', params);
      if (!parseInt(res.status)) {
        commit('update', state.hospitalList.concat(res.result));
      }
    } catch (e) {
      console.log(e);
    }
  },
  async search({ commit }, searchText) {
    commit('search', searchText);
  },
};

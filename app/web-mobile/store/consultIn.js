import $C from '../plugins/$C';
import helper from '../plugins/helper';

export const state = () => ({
  consultInList: [],
});

export const mutations = {
  update(state, consultInList) {
    state.consultInList = consultInList;
  },
  clean(state) {
    state.consultInList = [];
  },
};

export const actions = {
  async refresh({ commit, rootState }) {
    try {
      const res = await helper.POST($C.medSys + 'v1/consult/dispose/list', {
        token: rootState.corsuser.user.token,
        pageNo: '1',
        pageSize: $C.pageSize,
      });
      if (!parseInt(res.status)) {
        commit('update', res.result);
      }
    } catch (e) {
      console.log(e);
    }
  },
  async loadMore({ commit, rootState, state }, index) {
    try {
      const res = await helper.POST($C.medSys + 'v1/consult/dispose/list', {
        token: rootState.corsuser.user.token,
        pageNo: index + 1,
        pageSize: $C.pageSize,
      });
      if (!parseInt(res.status)) {
        commit('update', state.consultInList.concat(res.result));
      }
    } catch (e) {
      console.log(e);
    }
  },
};

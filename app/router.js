module.exports = app => {
  app.get('/home', app.controller.home.index);
  app.get('/customer/authorize/captcha', app.controller.customer.authorize.captcha);
  app.post('/customer/authorize/login', app.controller.customer.authorize.login);

  // not use path /cn and /public as it server for static source by default
};

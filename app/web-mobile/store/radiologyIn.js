import $C from '../plugins/$C';
import helper from '../plugins/helper';

export const state = () => ({
  radiologyInList: [],
});

export const mutations = {
  update(state, radiologyInList) {
    state.radiologyInList = radiologyInList;
  },
  clean(state) {
    state.radiologyInList = [];
  },
};

export const actions = {
  async refresh({ commit, rootState }) {
    try {
      const res = await helper.POST($C.medSys + 'v1/diagnose/exams/myDiagList', {
        token: rootState.corsuser.user.token,
        pageNo: '1',
        pageSize: $C.pageSize,
      });
      if (!parseInt(res.status)) {
        commit('update', res.result);
      }
    } catch (e) {
      console.log(e);
    }
  },
  async loadMore({ commit, rootState, state }, index) {
    try {
      const res = await helper.POST($C.medSys + 'v1/diagnose/exams/myDiagList', {
        token: rootState.corsuser.user.token,
        pageNo: index + 1,
        pageSize: $C.pageSize,
      });
      if (!parseInt(res.status)) {
        commit('update', state.radiologyInList.concat(res.result));
      }
    } catch (e) {
      console.log(e);
    }
  },
};
